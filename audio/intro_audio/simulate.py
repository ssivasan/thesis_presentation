import scipy.io
import matplotlib.pylab as plt
from ipdb import set_trace
import numpy as np
from fftfilt import fftfilt
import soundfile as sf

set_trace()
filename = 'target.wav'
mat = scipy.io.loadmat('air_binaural_aula_carolina_1_1_1_90_3.mat')
rir = mat['h_air'][0]
data, fs = sf.read(filename)
male, fs = sf.read('male_16k.wav', always_2d=True)

data_mix = male[:len(data), 0]


sword, _ = sf.read('sword_fight.wav')

sword = sword[:len(data),0]

rev_data = fftfilt(rir, data)
data_mix = fftfilt(rir, data_mix)

rev_target =  rev_data + 0.1 * sword + 1.2 * data_mix

rev_target = np.sum(data**2)/np.sum(rev_target**2) * rev_target
rev_target = np.clip(rev_target, -0.7, 0.7)
sf.write('rev.wav', rev_target, fs)
